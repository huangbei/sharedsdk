package com.bigbigcloud.sharedsdk.wechat;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.Toast;

import com.bigbigcloud.sharedsdk.Platform;
import com.bigbigcloud.sharedsdk.PlatformActionListener;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.modelmsg.SendAuth;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/5/19.
 */
public class Wechat extends Platform {

    private final static String GET_TOOKEN_API = "https://api.weixin.qq.com/sns/oauth2/access_token";
    private final static String GET_USER_INFO_API = "https://api.weixin.qq.com/sns/userinfo";

    private static String NAME = Wechat.class.getSimpleName();
    private int version = 1;

    private String AppId;
    private String AppSecret;
    private String RedirectUri;

    private HashMap<String, String> params;
    private  Map<String, Object> result_map = new HashMap<String, Object>();

    private IWXAPI api;


    private String tag = NAME;

    public Wechat(Activity context) {
        super(context);
        Log.d(tag, " Wechat construct !!!");
    }

    @Override
    public void getUserInfo() {

    }

    @Override
    protected void initDevInfo() {

    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public int getVersion() {
        return 1;
    }

    @Override
    public int getPlatformId() {
        return WECHAT_LOGIN;
    }

    public String getAppId() {
        return AppId;
    }

    @Override
    public void setParams(HashMap<String, String> params) {
        this.params = params;
        if(params!=null){
            this.AppId = params.get("AppId");
            this.AppSecret = params.get("AppSecret");
            this.RedirectUri = params.get("RedirectUri");
            api = WXAPIFactory.createWXAPI(getContext(),
                    AppId, false);
            api.registerApp(AppId);
            Log.d(tag, " set params appid " + AppId);
        }
    }

    @Override
    public void doAuthorize() {
        if (!checkApkExist(context, "com.tencent.mm")) {
            Toast.makeText(context, "请先安装微信客户端再操作", Toast.LENGTH_SHORT).show();
            listener.onCancel(this, 1);
        }
        SendAuth.Req req = new SendAuth.Req();
        req.scope = "snsapi_userinfo";
        req.state = "yangyang";
        boolean state = api.sendReq(req);
        Log.d(tag, " wechat do authorize !!! state = " + state);
    }

    @Override
    protected boolean checkAuthorize(int var1, Object var2) {
        return false;
    }

    @Override
    protected void userInfo(String uid) {

    }

    @Override
    public Context getContext() {
        return super.getContext();
    }

    @Override
    public void setPlatformActionListener(PlatformActionListener listener) {
        super.setPlatformActionListener(listener);
    }

    @Override
    public PlatformActionListener getListener() {
        return super.getListener();
    }

    public  void setWeiXinAuthResult(int errCode, String code) {
        if (listener == null) {
            return;
        }
        switch (errCode) {
            case BaseResp.ErrCode.ERR_OK:
                getWinXinToken(code);
                break;
            case BaseResp.ErrCode.ERR_USER_CANCEL:
                listener.onCancel(this, 1);
                break;
            case BaseResp.ErrCode.ERR_AUTH_DENIED:
                break;
            default:
                break;
        }
    }

    private  void getWinXinToken(final String code){
        String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid="
                + AppId + "&secret="
                + AppSecret + "&code="
                + code + "&grant_type=authorization_code";
        RequestParams requestParams = new RequestParams(url);
        x.http().get(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Log.d(tag, "getWinXinToken result " + result);
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(result);
                    if (jsonObject.has("errcode")) {
                        result_map.put("error_code", jsonObject.getString("errcode"));
                        result_map.put("error_message", jsonObject.getString("errmsg"));
                        listener.onError(Wechat.this, 0,null);
                    } else {
                        String openid = jsonObject.getString("openid");
                        String token = jsonObject.getString("access_token");
                        platformDb.putUserId(openid);
                        platformDb.putToken(token);
                        getWeiXinUserInfo(openid, token);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            private  void getWeiXinUserInfo(final String open_id, final String access_token){
                String url = "https://api.weixin.qq.com/sns/userinfo?openid=" + open_id +
                        "&access_token="+access_token;
                RequestParams requestParams = new RequestParams(url);
                x.http().get(requestParams, new CommonCallback<String>() {
                    @Override
                    public void onSuccess(String result) {
                        Log.d(tag, "getWeiXinUserInfo result " + result);
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(result);
                            result_map.put("name", jsonObject.getString("nickname"));
                            result_map.put("imgurl", jsonObject.getString("headimgurl"));
                            listener.onComplete(Wechat.this, 0, result_map);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable ex, boolean isOnCallback) {

                    }

                    @Override
                    public void onCancelled(CancelledException cex) {

                    }

                    @Override
                    public void onFinished() {

                    }
                });
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    /**
     * @param context
     * @param packageName
     * @return
     */
    public static boolean checkApkExist(Context context, String packageName) {
        if (packageName == null || "".equals(packageName))
            return false;
        try {
            ApplicationInfo info = context.getPackageManager()
                    .getApplicationInfo(packageName,
                            PackageManager.GET_UNINSTALLED_PACKAGES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
