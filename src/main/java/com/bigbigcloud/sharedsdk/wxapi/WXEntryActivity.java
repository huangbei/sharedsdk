package com.bigbigcloud.sharedsdk.wxapi;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.bigbigcloud.sharedsdk.ShareSDK;
import com.bigbigcloud.sharedsdk.wechat.Wechat;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.modelmsg.SendAuth;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;


public class WXEntryActivity extends Activity implements IWXAPIEventHandler {

    private String TAG = "WXEntryActivity";
    private IWXAPI api;
    private Wechat wechat;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        wechat = (Wechat) ShareSDK.getPlatform("Wechat");
        api = WXAPIFactory.createWXAPI(this,
                wechat.getAppId(), false);
        api.handleIntent(getIntent(), this);
        Log.d(TAG, " WXEntryActivity oncreate ");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        setIntent(intent);
        api.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq baseReq) {

    }

    @Override
    public void onResp(BaseResp resp) {
        Log.d(TAG, " WXEntryActivity on Resp !!!");
        SendMessageToWX res;
        if (resp instanceof SendMessageToWX.Resp) {
            finish();
            return;
        }
        String code = null;
        switch (resp.errCode) {
            case BaseResp.ErrCode.ERR_OK:
                SendAuth.Resp sendResp = (SendAuth.Resp) resp;
                code = sendResp.code;
                Log.d(TAG, " get code " + code);
                break;
            case BaseResp.ErrCode.ERR_USER_CANCEL:
                break;
            case BaseResp.ErrCode.ERR_AUTH_DENIED:
                break;
            default:
                break;
        }
        wechat.setWeiXinAuthResult(resp.errCode, code);
        finish();
        //  Toast.makeText(this, result + " ", Toast.LENGTH_LONG).show();
    }


}