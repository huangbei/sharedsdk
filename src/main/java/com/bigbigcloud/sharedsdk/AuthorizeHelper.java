package com.bigbigcloud.sharedsdk;

import android.webkit.WebViewClient;

/**
 * Created by abc on 15/9/24.
 */
public interface AuthorizeHelper {

    Platform getPlatform();

    String getAuthorizeUrl();

    String getRedirectUri();

    WebViewClient getWebViewClient(FakeChildActivity fakeChildActivity);
}
