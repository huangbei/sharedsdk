package com.bigbigcloud.sharedsdk.qq;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.bigbigcloud.sharedsdk.FakeChildUIActivity;
import com.bigbigcloud.sharedsdk.Platform;
import com.bigbigcloud.sharedsdk.PlatformActionListener;
import com.tencent.connect.UserInfo;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/5/19.
 */
public class QQ extends Platform implements IUiListener{

    private static String NAME = QQ.class.getSimpleName();
    private int version = 1;

    private String AppId;
    private String AppKey;
    private String RedirectUri;

    private HashMap<String, String> params;

    private QQAuthorizeHelperUtil qqAuthorizeHelperUtil;

    public Tencent mTencent;
    private Map<String, Object> result_map = new HashMap<String, Object>();

    private String tag = "yang";

    public QQ(Activity context) {
        super(context);
        Log.i(tag, " QQ construct !!!");
        qqAuthorizeHelperUtil = new QQAuthorizeHelperUtil(this);
    }

    @Override
    public void getUserInfo() {

    }

    @Override
    protected void initDevInfo() {

    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public int getVersion() {
        return version;
    }

    @Override
    public int getPlatformId() {
        return QQ_LOGIN;
    }

    @Override
    public void setParams(HashMap<String, String> params) {
        this.params = params;
        if(params!=null){
            this.AppId = params.get("AppId");
            this.AppKey = params.get("AppKey");
            this.RedirectUri = params.get("RedirectUri");
            Log.d("YANG", " APPID " + AppId);
            mTencent = Tencent.createInstance(AppId, context.getApplicationContext());
        }
    }

    @Override
    public void doAuthorize() {
        if(mTencent.isSupportSSOLogin(context)){
            Log.d(tag, " sso login qq");
            mTencent.login(context, "all", this);
        }else {
            Log.d(tag, " normal web login qq");
            final FakeChildUIActivity fakeChildUIActivity = new FakeChildUIActivity(qqAuthorizeHelperUtil);
            fakeChildUIActivity.show(getContext(), null);
        }
    }

    @Override
    protected boolean checkAuthorize(int var1, Object var2) {
        return false;
    }

    @Override
    protected void userInfo(String uid) {

    }

    @Override
    public Context getContext() {
        return super.getContext();
    }

    @Override
    public void setPlatformActionListener(PlatformActionListener listener) {
        super.setPlatformActionListener(listener);
    }

    @Override
    public PlatformActionListener getListener() {
        return super.getListener();
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onComplete(Object response) {
        Log.d("yang", " on complete !!");
        if (response == null || listener == null || mTencent == null) {
            return;
        }
        Log.d(tag, " qq sso login response " + response);
        try {
            JSONObject jo = (JSONObject) response;
            int ret = jo.getInt("ret");
            if (ret == 0) {
                String openID = jo.getString("openid");
                String accessToken = jo.getString("access_token");
                platformDb.putUserId(openID);
                platformDb.putToken(accessToken);
                result_map.put("uid", openID);
                result_map.put("accessToken", accessToken);
                mTencent.setAccessToken(accessToken, jo.getString("expires_in"));
                mTencent.setOpenId(openID);
                UserInfo userInfo = new UserInfo(context, mTencent.getQQToken());
                userInfo.getUserInfo(new IUiListener() {
                    @Override
                    public void onComplete(Object json) {
                        if (json == null) return;
                        Log.d(tag, " userinf response " + json.toString());
                        JSONObject jsonObject = (JSONObject) json;
                        try {
                            String name = jsonObject.getString("nickname");
                            String imgUrl = jsonObject.getString("figureurl");
                            result_map.put("name", name);
                            result_map.put("imgurl", imgUrl);
                            platformDb.putUserName(name);
                            platformDb.putUserIcon(imgUrl);
                            listener.onComplete(QQ.this, 0 ,result_map);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(UiError uiError) {

                    }

                    @Override
                    public void onCancel() {

                    }
                });


            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onError(UiError uiError) {

    }

    public String getAppId() {
        return AppId;
    }

    public void setAppId(String appId) {
        AppId = appId;
    }

    public String getAppKey() {
        return AppKey;
    }

    public void setAppKey(String appKey) {
        AppKey = appKey;
    }

    public String getRedirectUri() {
        return RedirectUri;
    }

    public void setRedirectUri(String redirectUri) {
        RedirectUri = redirectUri;
    }
}
