package com.bigbigcloud.sharedsdk.qq;

import android.graphics.Bitmap;
import android.util.Log;
import android.webkit.WebView;

import com.bigbigcloud.sharedsdk.BbcWebViewClient;
import com.bigbigcloud.sharedsdk.FakeChildActivity;
import com.bigbigcloud.sharedsdk.ParamUtils;
import com.bigbigcloud.sharedsdk.PlatformDb;

import java.util.HashMap;

/**
 * Created by Administrator on 2016/5/20.
 */
public class QQWebViewClient extends BbcWebViewClient {

    private String tag = "QQWebViewClient";

    private QQAuthorizeHelperUtil qqAuthorizeHelperUtil;

    public QQWebViewClient(FakeChildActivity activity) {
        super(activity);
        qqAuthorizeHelperUtil = (QQAuthorizeHelperUtil)fakeChildActivity.getAuthorizeHelper();
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        Log.d(tag, "shouldOverrideUrlLoading url : " + url);
        if(url.startsWith(redirectUri)){
            int start = url.indexOf("#");
            if(start != -1){
                String content = url.substring(start + 1);
                HashMap<String, String> params = ParamUtils.decodeUrl(content);
                String token = params.get("access_token");
                long expiresIn = Long.valueOf(params.get("expires_in"));
                PlatformDb db = qqAuthorizeHelperUtil.getPlatform().getPlatformDb();
                db.putToken(token);
                db.putExpiresIn(expiresIn);
                qqAuthorizeHelperUtil.getOpenId((token));
                fakeChildActivity.finish();
            }
        }
        return super.shouldOverrideUrlLoading(view, url);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        Log.d(tag, "onPageStarted url : " + url);
        super.onPageStarted(view, url, favicon);
    }
}
