package com.bigbigcloud.sharedsdk.qq;

import android.util.Log;
import android.webkit.WebViewClient;

import com.bigbigcloud.sharedsdk.AuthorizeHelperUtil;
import com.bigbigcloud.sharedsdk.FakeChildActivity;
import com.bigbigcloud.sharedsdk.Platform;
import com.bigbigcloud.sharedsdk.PlatformDb;

import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.util.HashMap;
import java.util.Map;


/**
 * User: (1203596603@qq.com)
 * Date: 2015-09-26
 * Time: 15:58
 * Version 1.0
 */

public class QQAuthorizeHelperUtil extends AuthorizeHelperUtil {

    private QQ qq;
    private PlatformDb platformDb;
    private Map<String, Object> result_map = new HashMap<String, Object>();

    private String tag = "QQAuthorizeHelperUtil";

    public QQAuthorizeHelperUtil(Platform platform) {
        super(platform);
        qq = (QQ)platform;
        platformDb = qq.getPlatformDb();
    }

    @Override
    public void getToken(String code) {

    }

    @Override
    public String getAuthorizeUrl() {
        String url = "https://graph.qq.com/oauth2.0/authorize" +
                "?response_type=token" +
                "&client_id=" + qq.getAppId() +
                "&redirect_uri=" + this.getRedirectUri() +
                "&scope=get_user_info,get_simple_userinfo,get_user_profile";
        Log.i(tag, "====" + url);
        return url;
    }

    @Override
    public WebViewClient getWebViewClient(FakeChildActivity fakeChildActivity) {
        return new QQWebViewClient(fakeChildActivity);
    }

    @Override
    public void userInfo(String uid) {
        String url = "https://graph.qq.com/user/get_simple_userinfo?access_token="
            + platformDb.getToken() + "&oauth_consumer_key="
            + qq.getAppId() + "&openid=" + uid;
        RequestParams requestParams = new RequestParams(url);
        x.http().get(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Log.d(tag, " get user info sucees " + result);
                try {
                    JSONObject object = new JSONObject(result);
                    String name = object.optString("nickname");
                    String imgUrl = object.optString("figureurl");
                    result_map.put("name", name);
                    result_map.put("imgurl", imgUrl);
                    platformDb.putUserName(name);
                    platformDb.putUserIcon(imgUrl);

                    qq.getListener().onComplete(qq, 0, result_map);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    @Override
    public String getRedirectUri() {
        return qq.getRedirectUri();
    }

    public void getOpenId(String token){
        String url = "https://graph.qq.com/oauth2.0/me?access_token=" + token;
        Log.i(tag, "==getOpenId==" + url);
        RequestParams requestParams = new RequestParams(url);
        x.http().get(requestParams, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Log.d(tag, " get open id result " + result);
                try {
                    int start = result.indexOf("{");
                    int end = result.indexOf("}");
                    String jsonStr = result.substring(start, end + 1);
                    Log.d(tag, "json string " + jsonStr);
                    JSONObject object = new JSONObject(jsonStr);
                    String openId = object.optString("openid");
                    if(openId != null){
                        platformDb.putUserId(openId);
                        userInfo(openId);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    public String getState(){
        return "QQ";
    }
}
