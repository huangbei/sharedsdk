package com.bigbigcloud.sharedsdk.sina;

import android.graphics.Bitmap;
import android.util.Log;
import android.webkit.WebView;

import com.bigbigcloud.sharedsdk.BbcWebViewClient;
import com.bigbigcloud.sharedsdk.FakeChildActivity;
import com.bigbigcloud.sharedsdk.ParamUtils;

import java.util.HashMap;

/**
 * Created by Administrator on 2016/5/19.
 */
public class SinaWebViewClient extends BbcWebViewClient {

    private String tag = "SinaWebViewClient";

    private SinaWeiboAuthorizeHelperUtil sinaWeiboAuthorizeHelperUtil;

    public SinaWebViewClient(FakeChildActivity activity) {
        super(activity);
        sinaWeiboAuthorizeHelperUtil = (SinaWeiboAuthorizeHelperUtil)fakeChildActivity.getAuthorizeHelper();
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        Log.d(tag, "shouldOverrideUrlLoading url : " + url);
        if(url.startsWith(redirectUri)) {
            HashMap<String,String> params = ParamUtils.decodeUrl(url);
            sinaWeiboAuthorizeHelperUtil.getToken(params.get("code"));
            fakeChildActivity.finish();
        }
        return super.shouldOverrideUrlLoading(view, url);
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        Log.d(tag, "onPageStarted url : " + url);
        super.onPageStarted(view, url, favicon);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
    }
}
