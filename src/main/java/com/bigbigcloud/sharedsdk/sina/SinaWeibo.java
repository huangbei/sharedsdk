package com.bigbigcloud.sharedsdk.sina;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.bigbigcloud.sharedsdk.Platform;
import com.bigbigcloud.sharedsdk.PlatformActionListener;
import com.sina.weibo.sdk.auth.AuthInfo;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import com.sina.weibo.sdk.exception.WeiboException;
import com.sina.weibo.sdk.net.RequestListener;

import org.json.JSONException;
import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.util.HashMap;



public class SinaWeibo extends Platform {

    public static final String NAME = SinaWeibo.class.getSimpleName();

    public static final String SCOPE = "email,direct_messages_read,direct_messages_write,"
            + "friendships_groups_read,friendships_groups_write,statuses_to_me_read,"
            + "follow_app_official_microblog," + "invitation_write";

    private int version = 1;

    private HashMap<String, String> params;

    private String AppKey;
    private String AppSecret;
    private String RedirectUrl;

    private SinaWeiboAuthorizeHelperUtil sinaWeiboAuthorizeHelperUtil;

    private AuthInfo mAuthInfo;
    private Oauth2AccessToken mAccessToken;
    private SsoHandler mSsoHandler;

    private String tag = "SinaWeibo";

    public SinaWeibo(Activity context) {
        super(context);
        Log.i(tag, " SinaWeibo construct !!!");
        sinaWeiboAuthorizeHelperUtil = new SinaWeiboAuthorizeHelperUtil(this);
    }

    @Override
    public void getUserInfo() {
        sinaWeiboAuthorizeHelperUtil.userInfo(null);
    }

    @Override
    protected void initDevInfo() {

    }

    @Override
    public void setPlatformActionListener(PlatformActionListener listener) {
        super.setPlatformActionListener(listener);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public int getVersion() {
        return version;
    }

    @Override
    public int getPlatformId() {
        return WEIBO_LOGGIN;
    }

    @Override
    public void setParams(HashMap<String, String> params) {
        this.params = params;
        if(params!=null){
            this.AppKey = params.get("AppKey");
            this.AppSecret = params.get("AppSecret");
            this.RedirectUrl = params.get("RedirectUrl");
        }
    }

    @Override
    public void doAuthorize() {
        mAuthInfo = new AuthInfo(context, AppKey,
                getRedirectUrl(), SCOPE);
        mSsoHandler = new SsoHandler(context, mAuthInfo);
        setSsoHandler(mSsoHandler);
        mSsoHandler.authorize(new WeiboAuthListener() {
            @Override
            public void onComplete(Bundle values) {
                Log.i("sina----", values.toString());
                mAccessToken = Oauth2AccessToken.parseAccessToken(values);
                if (mAccessToken.isSessionValid()) {
                    platformDb.putToken(mAccessToken.getToken());
                    platformDb.putUserId(mAccessToken.getUid());
                    platformDb.putExpiresIn(mAccessToken.getExpiresTime());
                    userInfo(mAccessToken.getUid());
                } else {
                    String code = values.getString("code");
                    listener.onError(SinaWeibo.this, 0, null);
                }
            }

            @Override
            public void onWeiboException(WeiboException e) {

            }

            @Override
            public void onCancel() {
                listener.onCancel(SinaWeibo.this, 0);
            }
        });

    }

    @Override
    protected boolean checkAuthorize(int var1, Object var2) {
        return false;
    }

    public String getAppKey() {
        return AppKey;
    }

    public void setAppKey(String appKey) {
        AppKey = appKey;
    }

    public String getAppSecret() {
        return AppSecret;
    }

    public void setAppSecret(String appSecret) {
        AppSecret = appSecret;
    }

    public String getRedirectUrl() {
        return RedirectUrl;
    }

    @Override
    public void userInfo(String uid) {
        String url = "https://api.weibo.com/2/users/show.json";
        RequestParams params = new RequestParams(url);
        params.addParameter("access_token", platformDb.getToken());
        params.addParameter("uid", TextUtils.isEmpty(uid) ? platformDb.getUserId() : uid);

        x.http().get(params, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Log.d(tag, " get user info result " + result);
                HashMap<String, Object> params = new HashMap<String, Object>();
                try{
                    JSONObject object = new JSONObject(result);
                    params.put("name", object.opt("name"));
                    params.put("imgurl", object.opt("profile_image_url"));
                    Log.d(tag, "get user info params : " + params.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
                if (listener != null) {
                    Log.d(tag, " get info on success");
                    listener.onComplete(SinaWeibo.this, 0, params);
                }
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    public void setRedirectUrl(String redirectUrl) {
        RedirectUrl = redirectUrl;
    }

    private class LogOutRequestListener implements RequestListener {
        @Override
        public void onComplete(String response) {
            if (!TextUtils.isEmpty(response)) {
                try {
                    JSONObject obj = new JSONObject(response);
                    String value = obj.getString("result");
                    if ("true".equalsIgnoreCase(value)) {
                        //AccessTokenKeeper.clear(getmActivity());
                       // logoutAuthListener.logoutAuthSuccess();
                        return;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //logoutAuthListener.logoutAuthError();

            }
        }

        @Override
        public void onWeiboException(WeiboException e) {
           // logoutAuthListener.logoutAuthError();
        }
    }
}
