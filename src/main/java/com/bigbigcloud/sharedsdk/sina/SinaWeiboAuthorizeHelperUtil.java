package com.bigbigcloud.sharedsdk.sina;

import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebViewClient;

import com.bigbigcloud.sharedsdk.AuthorizeHelperUtil;
import com.bigbigcloud.sharedsdk.FakeChildActivity;
import com.bigbigcloud.sharedsdk.Platform;
import com.bigbigcloud.sharedsdk.PlatformDb;

import org.json.JSONObject;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.util.HashMap;


/**
 * User: (1203596603@qq.com)
 * Date: 2015-09-24
 * Time: 11:40
 * Version 1.0
 */

public class SinaWeiboAuthorizeHelperUtil extends AuthorizeHelperUtil {

    private SinaWeibo sinaWeibo;
    private PlatformDb db;
    private String tag = "SinaWeiboAuthorizeHelperUtil";


    public SinaWeiboAuthorizeHelperUtil(Platform platform) {
        super(platform);
        sinaWeibo = (SinaWeibo)platform;
        db = sinaWeibo.getPlatformDb();
    }

    @Override
    public String getAuthorizeUrl() {
        String authorizeUrl = "https://api.weibo.com/oauth2/authorize" +
                "?client_id=" +
                sinaWeibo.getAppKey() +
                "&redirect_uri=" +
                sinaWeibo.getRedirectUrl();
        Log.i(tag, "---" + authorizeUrl);
        return authorizeUrl;
    }

    @Override
    public String getRedirectUri() {
        return sinaWeibo.getRedirectUrl();
    }

    @Override
    public WebViewClient getWebViewClient(FakeChildActivity fakeChildActivity) {
        return new SinaWebViewClient(fakeChildActivity);
    }

    @Override
    public void getToken(String code) {
        Log.d(tag," get token code : " + code);
        String url = "https://api.weibo.com/oauth2/access_token";
        RequestParams params = new RequestParams(url);
        params.addParameter("client_id", sinaWeibo.getAppKey());
        params.addParameter("client_secret", sinaWeibo.getAppSecret());
        params.addParameter("grant_type", "authorization_code");
        params.addParameter("redirect_uri", sinaWeibo.getRedirectUrl());
        params.addParameter("code", code);

        x.http().post(params, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Log.d(tag, " getToken result " + result);
                try {
                    JSONObject object = new JSONObject(result);
                    String access_token = object.optString("access_token");
                    long expires_in = object.optLong("expires_in");
                    String uid = object.optString("uid");
                    db.putToken(access_token);
                    db.putExpiresIn(expires_in);
                    db.putUserId(uid);
                    userInfo(uid);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    @Override
    public void userInfo(String uid) {
        String url = "https://api.weibo.com/2/users/show.json";
        RequestParams params = new RequestParams(url);
        params.addParameter("access_token", db.getToken());
        params.addParameter("uid", TextUtils.isEmpty(uid) ? db.getUserId() : uid);

        x.http().get(params, new Callback.CommonCallback<String>() {
            @Override
            public void onSuccess(String result) {
                Log.d(tag, " get user info result " + result);
                HashMap<String, Object> params = new HashMap<String, Object>();
                try{
                    JSONObject object = new JSONObject(result);
                    params.put("name", object.opt("name"));
                    params.put("imgurl", object.opt("profile_image_url"));
                    Log.d(tag, "get user info params : " + params.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
                if (sinaWeibo.getListener() != null) {
                    Log.d(tag, " get info on success");
                    sinaWeibo.getListener().onComplete(getPlatform(), 0, params);
                }
            }

            @Override
            public void onError(Throwable ex, boolean isOnCallback) {

            }

            @Override
            public void onCancelled(CancelledException cex) {

            }

            @Override
            public void onFinished() {

            }
        });
    }


}
