package com.bigbigcloud.sharedsdk;

import android.webkit.WebViewClient;

/**
 * Created by Administrator on 2016/5/19.
 */
public abstract class BbcWebViewClient extends WebViewClient {
    protected FakeChildActivity fakeChildActivity;
    protected String redirectUri;

    public BbcWebViewClient(FakeChildActivity activity){
        this.fakeChildActivity = activity;
        redirectUri = fakeChildActivity.getAuthorizeHelper().getRedirectUri();
    }

}
