package com.bigbigcloud.sharedsdk;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.bigbigcloud.sharedsdk.qq.QQ;
import com.bigbigcloud.sharedsdk.sina.SinaWeibo;
import com.bigbigcloud.sharedsdk.wechat.Wechat;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;


public class ShareSDK {

    private static HashMap<String, HashMap<String, String>> platformHashMapStr = new HashMap<String, HashMap<String, String>>();

    private String tag = "ShareSDK";

    private static Map<String, Platform> platformMap = new HashMap<>();

    public ShareSDK(){

    }

    public static void initSDK(Activity context){
        //mXmlConfig = new XMLConfig(context);
        analyXML(context);
        SinaWeibo sinaWeibo = new SinaWeibo(context);
        sinaWeibo.setParams(platformHashMapStr.get(sinaWeibo.getName()));
        platformMap.put(sinaWeibo.getName(), sinaWeibo);
        QQ qq = new QQ(context);
        qq.setParams(platformHashMapStr.get(qq.getName()));
        platformMap.put(qq.getName(), qq);
        Wechat wechat = new Wechat(context);
        wechat.setParams(platformHashMapStr.get(wechat.getName()));
        platformMap.put(wechat.getName(), wechat);
    }

    public static Platform getPlatform(String platFromName){
        //return mXmlConfig.getPlatform(platFromName);
        return platformMap.get(platFromName);
    }

    private static void analyXML(Context context){
        InputStream inputStream = null;
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser pullParser = factory.newPullParser();

            inputStream = context.getAssets().open("ShareSDK.xml");

            pullParser.setInput(inputStream, "UTF-8");

            for(int eventType =pullParser.getEventType(); eventType != XmlPullParser.END_DOCUMENT; eventType = pullParser.next()){
                if(eventType == XmlPullParser.START_TAG){
                    String name = pullParser.getName();
                    HashMap<String, String> attrNameValue = new HashMap();
                    Log.i("yang", "---43---" + name);
                    int attrCount = pullParser.getAttributeCount();
                    for(int j=0; j<attrCount; ++j){
                        String attrName = pullParser.getAttributeName(j);
                        String attrValue = pullParser.getAttributeValue(j).trim();
                        Log.i("yang", "---49---" + attrName + ":" + attrValue);
                        attrNameValue.put(attrName, attrValue);
                    }
                    platformHashMapStr.put(name, attrNameValue);
                }
            }

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(inputStream!=null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }



}
