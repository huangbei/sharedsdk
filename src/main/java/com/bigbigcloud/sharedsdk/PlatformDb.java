package com.bigbigcloud.sharedsdk;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Administrator on 2016/5/20.
 */
public class PlatformDb {
    private static final String DB_NAME = "bbc_sharesdk_db";
    private SharedPreferences db;
    private String platformNname;
    private int platformVersion;

    public PlatformDb(Context context, String name, int version) {
        String dbName = DB_NAME + name + "_" + version;
        this.db = context.getSharedPreferences(dbName, Context.MODE_PRIVATE);
        this.platformNname = name;
        this.platformVersion = version;
    }

    public void put(String key, String value) {
        SharedPreferences.Editor editor = this.db.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String get(String key) {
        return this.db.getString(key, "");
    }

    public String getToken() {
        return this.db.getString("token", "");
    }

    public void putToken(String token) {
        put("token", token);
    }

    public String getTokenSecret() {
        return this.db.getString("secret", "");
    }

    public void putTokenSecret(String secret) {
        put("secret", secret);
    }

    public long getExpiresIn() {
        long expiresIn = 0L;
        try {
            expiresIn = this.db.getLong("expiresIn", 0L);
        } catch (Throwable var6) {
            try {
                expiresIn = (long)this.db.getInt("expiresIn", 0);
            } catch (Throwable var5) {
                expiresIn = 0L;
            }
        }
        return expiresIn;
    }

    public void putExpiresIn(long expires) {
        SharedPreferences.Editor editor = this.db.edit();
        editor.putLong("expiresIn", expires);
        editor.putLong("expiresTime", System.currentTimeMillis());
        editor.commit();
    }

    public long getExpiresTime() {
        long expiresTime = this.db.getLong("expiresTime", 0L);
        long expiresIn = this.getExpiresIn();
        return expiresTime + expiresIn * 1000L;
    }

    public int getPlatformVersion() {
        return this.platformVersion;
    }

    public String getPlatformNname() {
        return this.platformNname;
    }

    public void putUserId(String platformId) {
        put("userID", platformId);
    }

    public String getUserId() {
        return this.db.getString("userID", "");
    }

    public void putUserName(String name) {put("nickname", name);}

    public String getUserName() {
        return this.db.getString("nickname", "");
    }

    public void putUserIcon(String iconUrl){put("icon", iconUrl);}

    public String getUserIcon() {
        return this.db.getString("icon", "");
    }

    public void removeAccount() {
        ArrayList list = new ArrayList();
        Iterator iterator = this.db.getAll().entrySet().iterator();

        while(iterator.hasNext()) {
            Map.Entry entry = (Map.Entry)iterator.next();
            list.add(entry.getKey());
        }
        SharedPreferences.Editor editor = this.db.edit();
        Iterator iterator1 = list.iterator();

        while(iterator1.hasNext()) {
            String key = (String)iterator1.next();
            editor.remove(key);
        }
        editor.commit();
    }
}
