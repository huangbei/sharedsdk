package com.bigbigcloud.sharedsdk;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.sina.weibo.sdk.auth.sso.SsoHandler;

import java.util.HashMap;


public abstract class Platform{

    public static final int NORMAL_LOGIN = 0;
    public static final int WEIBO_LOGGIN = 1;
    public static final int QQ_LOGIN = 2;
    public static final int WECHAT_LOGIN = 3;

    protected final Activity context;

    protected PlatformDb platformDb;

    private SsoHandler ssoHandler;//sina handler

    protected PlatformActionListener listener;

    public Platform(Activity context){
        this.context = context;
        platformDb = new PlatformDb(context, getName(), getVersion());
    }

    public PlatformDb getPlatformDb(){
        return platformDb;
    }

    public abstract void getUserInfo();

    protected abstract void initDevInfo();

    public abstract String getName();

    public abstract int getVersion();

    public abstract int getPlatformId();

    public abstract void setParams(HashMap<String, String> params);

    public abstract void doAuthorize();

    protected abstract boolean checkAuthorize(int var1, Object var2);

    protected abstract void userInfo(String uid);

    public Context getContext() {
        return this.context;
    }

    public void setPlatformActionListener(PlatformActionListener listener) {
        Log.i("Platform", "----49----"+this.listener);
        this.listener = listener;
    }

    public PlatformActionListener getListener() {
        Log.i("Platform", "----54----"+this.listener);
        return this.listener;
    }

    public SsoHandler getSsoHandler() {
        return ssoHandler;
    }

    public void setSsoHandler(SsoHandler ssoHandler) {
        this.ssoHandler = ssoHandler;
    }
}
