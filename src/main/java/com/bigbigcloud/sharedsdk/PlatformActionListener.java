package com.bigbigcloud.sharedsdk;

import java.util.Map;

/**
 * Created by abc on 15/9/24.
 */
public interface PlatformActionListener {

    void onComplete(Platform platform, int code, Map<String, Object> params);

    void onError(Platform platform, int code, Throwable throwable);

    void onCancel(Platform platform, int code);
}
