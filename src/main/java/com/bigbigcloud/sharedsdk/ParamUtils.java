package com.bigbigcloud.sharedsdk;

import java.util.HashMap;

/**
 * Created by Thomas on 2016/5/19.
 */
public class ParamUtils {

    public static HashMap<String,String> decodeUrl(String content){
        HashMap<String,String> params = new HashMap<String,String>();
        try {
            String decodeSource = "";
            if (content.indexOf("?") != -1) {
                decodeSource = content.substring(content.indexOf("?") + 1, content.length());
            } else {
                decodeSource = content;
            }
            String[] decodeParams = decodeSource.split("&");

            for (String keyValues : decodeParams) {
                String[] keyValue = keyValues.split("=");
                params.put(keyValue[0], keyValue[1]);
            }
        } catch (Exception e) {
        }
        return params;
    }

}
